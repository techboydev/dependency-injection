
// https://stackoverflow.com/questions/5986051/c-sharp-static-variables-scope-and-persistence

// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/static-classes-and-static-class-members#static-members

// https://stackoverflow.com/questions/194999/are-static-class-instances-unique-to-a-request-or-a-server-in-asp-net

// Dependency injection 

// When properties of class declare as static, it will be shared across all requests or exactly it will be share until the AppDomain(Application Domain) lives.

// # common issues

// There are multiple API requests triggers, if the API calls run asynchorize, since the load of work is heavy, you will run into error as below

// Cannot access a disposed object. 
// A common cause of this error is disposing a context that was resolved from dependency injection and 
// then later trying to use the same context instance elsewhere in your application. This may occur if you are calling Dispose() on the context, or wrapping the context in a using statement. 
// If you are using dependency injection, you should let the dependency injection container take care of disposing context instances.

// #######
// Even though the database context is declared per request(by default) or transient, the issue is not solved.

// Entity framework db context is not thread safe, however, separate thread(request is run by thread) should have their own db context instance.

// ############# Solutions

// 1. Keep the controller open in synchronize fashion to wait for the task finish, so the controller is declare to return a task but not void.
// 2. However, synchronize is not a choice for some instances, therefore, making sure there is no reference of any related classs declared as static property

// ########Startup.cs
//this is common DI


namespace Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
           
			//example DI
			services.AddScoped<ICustomLogger, Logger>();
			
			//....extra code..... and etc....
			
			//db
			 services.AddDbContext<MyExampleDbContext>(options =>
            {
                options.UseSqlServer(connection);
            },ServiceLifetime.Scoped);
		}
	}
}

// Your intended use class

public class NotificationHelper: INotifactionHelper
	{
		//correct output will be expected when DI injection get created by multiple requests api
		// task will be running in separate thread and DB instance will not be share.
		private ICustomLogger _logger;
		
		///wrong way to decleare, since static make _logger instance stay live until the Application terminated,
		//multiple read and write to DB using api separates api call will get conflicts since the DB connection within _logger instance
		//will remain the same coz it is declare static, the whole _logger instance will only be created once per application not requests
		//even api request instances and classs are scope per request. However, any static variables are declared once and share among ALL requests.
		
		
		//*****private static ICustomLogger _logger;
		
	}
	
	
public interface ICustomLogger
{
	void Log(string message, LogLevel loglevel, string triggeredBy = null, Exception exception = null, object data = null);

}
public class Logger :  ICustomLogger
{
	private MyExampleDbContext _db;
	
	public Logger(MyExampleDbContext db)
	{
		_db=db;
	}
	
	public void Log(string message, LogLevel loglevel, string triggeredBy = null, Exception exception = null, object data = null)
	{
		///some database actions....
		_db.SomeTableName.Add(new SomeTableName{
			//.....
		});
		_db.SaveChange();
		
	}
}