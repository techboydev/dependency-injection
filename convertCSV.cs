using System;
using System.IO;
using System.Linq;
namespace CSV
{
	class Program
	{
		static void Main(string[] args)
		{
			var historicalData = File.ReadAllLines(@"C:\Users\thienn\source\repos\CSV\CSV\student.csv");
			var content= from h in historicalData
						 let data = h.Split(',')
						 select new
						 {
							 FirstName = data[0],
							 Middle = data[1],
							 Surname = data[2],
							 YearLevel = data[3],
							 House = data[4],
							 Email = data[5],
							 ExternalCode = data[6],
							 RealYearLevel = data[7],
							 SchoolId= data[8]
						 };
			var classCode = File.ReadAllLines(@"C:\Users\thienn\source\repos\CSV\CSV\16852.csv");
			var row = from k in classCode
					  let data = k.Split(',',2)
					  select new {
						  Email =data[0],
						  ClassCode =data[1]
					  };
			foreach (var t in row)
			{
				//Console.WriteLine("{0} - {1}",t.Email,t.ClassCode);
			}
			Console.WriteLine("{0:30},\t{1:40},\t{2:40},{3:-50}", "FirstName", "L", "Y", "C");
			try
			{
				using (StreamWriter outputFile = new StreamWriter("t.csv"))
				{
					int i = 0;
					foreach (var r in content)
					{
						if (i == 0)
						{
							i++;
							continue;
						}

						var match = row.Where(x => x.Email.Contains(r.Email.ToLower())).FirstOrDefault();
						Console.WriteLine("{0},{1},{2},{3},{4},{5},\"{6}\"",
							r.SchoolId,
							r.YearLevel.Substring(r.YearLevel.IndexOf('r') + 1),
							r.Email,
							r.FirstName,
							r.Surname,
							r.ExternalCode,
							match.ClassCode
							);
						outputFile.WriteLine("{0},{1},{2},{3},{4},{5},\"{6}\"",
							r.SchoolId,
							r.YearLevel.Substring(r.YearLevel.IndexOf('r') + 1),
							r.Email,
							r.FirstName,
							r.Surname,
							r.ExternalCode,
							match.ClassCode);
						//match.Substring(match.IndexOf(',') + 1));
						i++;
						
					}
				}
			
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);

			}
}
	}
}
